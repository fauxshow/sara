#This code controls the main drive motors

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

pin1A = 22
pin1B = 27
pin2A = 24
pin2B = 23

moveSpeed = 60
turnSpeed = 60		#default speeds for moving and turning, these are a percentage 0-100

GPIO.setup(pin1A, GPIO.OUT)
GPIO.setup(pin1B, GPIO.OUT)
GPIO.setup(pin2A, GPIO.OUT)
GPIO.setup(pin2B, GPIO.OUT)

pwm1A = GPIO.PWM(pin1A, 50)
pwm1B = GPIO.PWM(pin1B, 50)
pwm2A = GPIO.PWM(pin2A, 50)
pwm2B = GPIO.PWM(pin2B, 50)

def move(motor, direction, speed):		#motor 1 or 2, direction: -1=CCW, 0=OFF, 1=CW
	if(motor == 1):
		if(direction == 1):		#CW
			pwm1B.ChangeDutyCycle(0)
			pwm1A.start(50)
			pwm1A.ChangeDutyCycle(speed)
		elif(direction == -1):	#CCW	
			pwm1A.ChangeDutyCycle(0)
			pwm1B.start(50)
			pwm1B.ChangeDutyCycle(speed)
		else:					#OFF
			pwm1A.ChangeDutyCycle(0)
			pwm1B.ChangeDutyCycle(0)
			pwm1A.stop()
			pwm1B.stop()
	elif(motor == 2):
		if(direction == 1):		#CW
			pwm2A.ChangeDutyCycle(0)
			pwm2B.start(50)
			pwm2B.ChangeDutyCycle(speed)
		elif(direction == -1):	#CCW	
			pwm2B.ChangeDutyCycle(0)
			pwm2A.start(50)
			pwm2A.ChangeDutyCycle(speed)
		else:					#OFF
			pwm2A.ChangeDutyCycle(0)
			pwm2B.ChangeDutyCycle(0)
			pwm2A.stop()
			pwm2B.stop()

def moveFullSpeed(motor, direction):		#motor 1 or 2, direction: -1=CCW, 0=OFF, 1=CW
	pinA = pin1A
	pinB = pin1B
	bit1 = 1
	bit2 = 0
	if(motor == 2):
		pinA = pin2A
		pinB = pin2B
		bit1 = 0
		bit2 = 1
	if(direction == 1):
		GPIO.output(pinA, bit1)	#CW
		GPIO.output(pinB, bit2)
	elif(direction == -1):
		GPIO.output(pinA, bit2)	#CCW
		GPIO.output(pinB, bit1)
	else:
		GPIO.output(pinA, 0)	#OFF
		GPIO.output(pinB, 0)

def stop():
	move(1, 0, 0)
	move(2, 0, 0)

def fwd(speed=moveSpeed):
	move(1, 1, speed)
	move(2, 1, speed)

def rev(speed=moveSpeed):
	move(1, -1, speed)
	move(2, -1, speed)

def left(speed=turnSpeed):
	move(1, -1, speed)
	move(2, 1, speed)

def right(speed=turnSpeed):
	move(1, 1, speed)
	move(2, -1, speed)

