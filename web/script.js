url = "http://"+ document.URL.split("/")[2] +"/";

count = 0

//fetch room data and update every second
setInterval(fetchRoomData,  2200);


wCounter = 30;

function fetchRoomData()
{
	//data received in as string "[inRoom],[lightLevel],[lampState],[lightState]"
	
	roomState = $.post(url+ "getRoomData/", function(data){
		updateRoomData(data);
	});
	
	//fetch webcam image
	$.post(url+ "getPic/", function(data){
		drawImage(data);
	});

	if(wCounter == 30)
	{
		fetchWeatherData();
		wCounter = 0;
	}
	wCounter++;
}

function fetchWeatherData()
{
	//data received in string "time, temp, pressure, lightLvl, humidity, rain"
	weatherData = $.post(url+ "getWeatherData/", function(data){
		updateWeatherData(data);
	});
}

function updateWeatherData(data)
{
	wData = data.split("|")

	displayString = "Temperature: "+wData[1]+"&deg;C<br>"+
		"Pressure: "+wData[2]+"mB<br>"+
		"Humidity: "+wData[4]+"%<br>"+
		"Light Level: "+wData[3]+"<br>"+
		"It is "+wData[5]+"<br><br>"+
		"<p>last updated: "+ wData[0] +"</p>";

	$("#weatherInfo").html(displayString);
}

function updateRoomData(data)
{
	roomData = data.split("|");
	inRoom = "";
	if(roomData[0] == '1')
		inRoom = "Room is occupied";
	else
		inRoom = "Room is empty";
	$("#occupiedInfo").text(inRoom);
	
	$("#lightLevel").text("Light level is: "+ roomData[1]);
	

	lamp = "";
	light = "";
	
	if(roomData[2] == "1")
		lamp = "Lamp is on";
	else
		lamp = "Lamp is off";
		
	if(roomData[3] == "1")
		light = "Light is on";
	else
		light = "Light is off";
	
	$("#lampInfo").text(lamp);
	$("#lightInfo").text(light);
	
	$("#tempInfo").text("Temperature is "+ roomData[4] +"\u00B0C");
	$("#humidityInfo").text("Humidity is "+ roomData[5] +"%");
}


//populate hour and minute boxes for time selection, also pulls in current alarms from databases and sets them
//also populates reminder stuff and sets to current time
function populateTimes()
{
	hours = "";
	minutes = "";
	for (var i=0; i<24; i++)
	{
		h = i;
		if (i<10)
			h = "0"+ i;
		hours += "<option>"+ h +"</option>";
	}
	for (var i=0; i<60; i++)
	{
		h = i;
		if (i<10)
			h = "0"+ i;
		minutes += "<option>"+ h +"</option>";
	}
	$("#monhbox").append(hours);
	$("#tuehbox").append(hours);
	$("#wedhbox").append(hours);
	$("#thuhbox").append(hours);
	$("#frihbox").append(hours);
	$("#sathbox").append(hours);
	$("#sunhbox").append(hours);
	
	$("#monmbox").append(minutes);
	$("#tuembox").append(minutes);
	$("#wedmbox").append(minutes);
	$("#thumbox").append(minutes);
	$("#frimbox").append(minutes);
	$("#satmbox").append(minutes);
	$("#sunmbox").append(minutes);
	
	$("#reminderHour").append(hours);
	$("#reminderMinute").append(minutes);
	
	//set reminder time to current time
	$("select#reminderHour").prop('selectedIndex', new Date().getHours());
	$("select#reminderMinute").prop('selectedIndex', new Date().getMinutes());
	
	getReminders();
	
	//pull in current alarm times from db
	alarmArray = [];
	alarms = $.post(url+ "fetchalarms/", function(data){
		setCurrentAlarmsValues(data);
	});

	//pull in current light states and strip colour from server and set UI accordingly
	$.post(url+ "getlightstates/", function(data){
		setLightStates(data);
		alert(data)
	});
	
}

function setCurrentAlarmsValues(data)
{
	alarmArray = data.split("|");
	sortedAlarms = [];		//sort array by day of week
	for(var i=0; i<alarmArray.length; i++)
	{
		day = alarmArray[i].split(",")[0];
		switch(day)
		{
			case('Monday'):
				sortedAlarms[0] = alarmArray[i];
				break;
			case('Tuesday'):
				sortedAlarms[1] = alarmArray[i];
				break;
			case('Wednesday'):
				sortedAlarms[2] = alarmArray[i];
				break;
			case('Thursday'):
				sortedAlarms[3] = alarmArray[i];
				break;
			case('Friday'):
				sortedAlarms[4] = alarmArray[i];
				break;
			case('Saturday'):
				sortedAlarms[5] = alarmArray[i];
				break;
			case('Sunday'):
				sortedAlarms[6] = alarmArray[i];
				break;
		}
	}
	
	$("select#monhbox").prop('selectedIndex', sortedAlarms[0].split(",")[1].split(":")[0]);
	$("select#monmbox").prop('selectedIndex', sortedAlarms[0].split(",")[1].split(":")[1]);
	
	$("select#tuehbox").prop('selectedIndex', sortedAlarms[1].split(",")[1].split(":")[0]);
	$("select#tuembox").prop('selectedIndex', sortedAlarms[1].split(",")[1].split(":")[1]);
	
	$("select#wedhbox").prop('selectedIndex', sortedAlarms[2].split(",")[1].split(":")[0]);
	$("select#wedmbox").prop('selectedIndex', sortedAlarms[2].split(",")[1].split(":")[1]);
	
	$("select#thuhbox").prop('selectedIndex', sortedAlarms[3].split(",")[1].split(":")[0]);
	$("select#thumbox").prop('selectedIndex', sortedAlarms[3].split(",")[1].split(":")[1]);
	
	$("select#frihbox").prop('selectedIndex', sortedAlarms[4].split(",")[1].split(":")[0]);
	$("select#frimbox").prop('selectedIndex', sortedAlarms[4].split(",")[1].split(":")[1]);
	
	$("select#sathbox").prop('selectedIndex', sortedAlarms[5].split(",")[1].split(":")[0]);
	$("select#satmbox").prop('selectedIndex', sortedAlarms[5].split(",")[1].split(":")[1]);
	
	$("select#sunhbox").prop('selectedIndex', sortedAlarms[6].split(",")[1].split(":")[0]);
	$("select#sunmbox").prop('selectedIndex', sortedAlarms[6].split(",")[1].split(":")[1]);
	
}

//handlers for changes of select box
$("#monhbox").change( function () {
	$.get(url+ "setalarm/Monday/"+ $("#monhbox").val() +":"+ $("#monmbox").val() +"/"); 
});
$("#monmbox").change( function () {
	$.get(url+ "setalarm/Monday/"+ $("#monhbox").val() +":"+ $("#monmbox").val() +"/"); 
});

$("#tuehbox").change( function () {
	$.get(url+ "setalarm/Tuesday/"+ $("#tuehbox").val() +":"+ $("#tuembox").val() +"/"); 
});
$("#tuembox").change( function () {
	$.get(url+ "setalarm/Tuesday/"+ $("#tuehbox").val() +":"+ $("#tuembox").val() +"/"); 
});

$("#wedhbox").change( function () {
	$.get(url+ "setalarm/Wednesday/"+ $("#wedhbox").val() +":"+ $("#wedmbox").val() +"/"); 
});
$("#wedmbox").change( function () {
	$.get(url+ "setalarm/Wednesday/"+ $("#wedhbox").val() +":"+ $("#wedmbox").val() +"/"); 
});

$("#thuhbox").change( function () {
	$.get(url+ "setalarm/Thursday/"+ $("#thuhbox").val() +":"+ $("#thumbox").val() +"/"); 
});
$("#thumbox").change( function () {
	$.get(url+ "setalarm/Thursday/"+ $("#thuhbox").val() +":"+ $("#thumbox").val() +"/"); 
});

$("#frihbox").change( function () {
	$.get(url+ "setalarm/Friday/"+ $("#frihbox").val() +":"+ $("#frimbox").val() +"/"); 
});
$("#frimbox").change( function () {
	$.get(url+ "setalarm/Friday/"+ $("#frihbox").val() +":"+ $("#frimbox").val() +"/"); 
});

$("#sathbox").change( function () {
	$.get(url+ "setalarm/Saturday/"+ $("#sathbox").val() +":"+ $("#satmbox").val() +"/"); 
});
$("#sathbox").change( function () {
	$.get(url+ "setalarm/Saturday/"+ $("#sathbox").val() +":"+ $("#satmbox").val() +"/"); 
});

$("#sunhbox").change( function () {
	$.get(url+ "setalarm/Sunday/"+ $("#sunhbox").val() +":"+ $("#sunmbox").val() +"/"); 
});
$("#sunmbox").change( function () {
	$.get(url+ "setalarm/Sunday/"+ $("#sunhbox").val() +":"+ $("#sunmbox").val() +"/"); 
});


//Reminders

$("#addReminder").click(function(){
	date  = $("#reminderDate").val();
	time =  date +"T"+ $("#reminderHour").val() +":"+ $("#reminderMinute").val();
	message = $("#reminderMessage").val();
	if(message != "" && date != "")
	{
		$.get(url+ "setreminder/"+time+"/"+message+"/");
		getReminders()
	}
});

function getReminders()
{
	reminders = $.post(url+ "fetchreminders/", function(data){
		populateReminders(data);
	});
}

function populateReminders(reminders)
{
	reminders = reminders.split("|");
	reminderBoxes = "";
	for(var i=0; i<reminders.length-1; i++)
	{	
		s = reminders[i].split(",");
		date = formatTime(s[0]);
		message = s[1];
		reminderBoxes += "<div class='reminderListBox'>"+date+"  "+message+" <button type='button' id='"+date+"' class='xBtn'>X</button></div><br>"
	}
	$("#reminderList").html(reminderBoxes);
}

function formatTime(d)
{
	timeString = d[8]+d[9]+"/"+d[5]+d[6]+"/"+d[2]+d[2]+" "+d[11]+d[12]+d[13]+d[14]+d[15];
	return timeString;
}

function drawImage(data)
{
	
	img = new Image();
	img.src = "data:image/jpeg;base64,"+data;
	var c=document.getElementById("roomImg");
	//c.innerHTML(data);
	var ctx=c.getContext("2d");
	ctx.drawImage(img,0,0);
}

//Switch Lamp
$("#lampOnBtn").click(function(){
	$.get(url+ "lamp/On");
});

$("#lampOffBtn").click(function(){
	$.get(url+ "lamp/Off");
});

//Switch Light
$("#lightOnBtn").click(function(){
	$.get(url+ "light/On");
});

$("#lightOffBtn").click(function(){
	$.get(url+ "light/Off");
});

//Switch Lamp to Auto
$("#lampAutoBtn").click(function(){
	$.get(url+ "lamp/auto");
});

//Switch Light to Auto
$("#lightAutoBtn").click(function(){
	$.get(url+ "light/auto");
});

//Set RGB strip colour
$("#setRGBColour").click(function(){
	colour = $("#redValue").val() +","+ $("#greenValue").val() +","+ $("#blueValue").val();
	$.get(url+ "rgbstrip/"+ colour);
});

//initilaise the light states buttons and RGB sliders with current state
function setLightStates(states)
{
	lightStates = states.split(",");
	if(lightStates[0] == "1")
		lamp = "Lamp Off";
	else
		lamp = "Lamp On";
		
	if(lightStates[1] == "1")
		light = "Light Off";
	else
		light = "Light On";
	
	$("#lampBtn").text(lamp);
	$("#lightBtn").text(light);
	
	$("#redValue").val(lightStates[2]);
	$("#greenValue").val(lightStates[3]);
	$("#blueValue").val(lightStates[4]);
}

//signal going to bed
$("#sleepBtn").click(function(){
	$.get(url+ "sleep");
});

//change the RGB strip colour and div background colour when the colour sliders are dragged
$("#redValue").change(function(){
	colour = $("#redValue").val() +","+ $("#greenValue").val() +","+ $("#blueValue").val();
	$("#setRGBColour").css('background-color', "rgb("+colour+")");
	$.get(url+ "rgbstrip/"+ colour);
});
$("#greenValue").change(function(){
	colour = $("#redValue").val() +","+ $("#greenValue").val() +","+ $("#blueValue").val();
	$("#setRGBColour").css('background-color', "rgb("+colour+")");
	$.get(url+ "rgbstrip/"+ colour);
});
$("#blueValue").change(function(){
	colour = $("#redValue").val() +","+ $("#greenValue").val() +","+ $("#blueValue").val();
	$("#setRGBColour").css('background-color', "rgb("+colour+")");
	$.get(url+ "rgbstrip/"+ colour);
});

//say alarm
$("#greet").click(function(){
	$.get(url+ "greeting/");
});

//say defined message
$("#sayBtn").click(function(){
	$.get(url+ "say/"+ $("#messageBox").val());
});
