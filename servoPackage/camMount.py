#control the camera mount servos
import servoDrive

yawNumber = 6
pitchNumber = 7

def yaw(pos):
	#any calibration can go here
	servoDrive.SetPos(yawNumber, pos)

def pitch(pos):
	#any calibration can go here
	servoDrive.SetPos(pitchNumber, pos)
