#!/usr/bin/python

from Adafruit_PWM_Servo_Driver import PWM
import time

# Initialise the PWM device using the default address
PWMdriver = PWM(0x40)

PWMdriver.setPWMFreq(60)

servoMin = 150  # Min pulse length out of 4096
servoMax = 600  # Max pulse length out of 4096

def setServoPulse(channel, pulse):
		pulseLength = 1000000                   # 1,000,000 us per second
		pulseLength /= 60                       # 60 Hz
		print "%d us per period" % pulseLength
		pulseLength /= 4096                     # 12 bits of resolution
		print "%d us per bit" % pulseLength
		pulse *= 1000
		pulse /= pulseLength
		pwm.setPWM(channel, 0, pulse)

def Map(deg, lowerfrom, upperfrom, lowerto, upperto):
	toRange = upperto - lowerto
	fromRange = upperfrom - lowerfrom
	ratio = (toRange*1.0)/fromRange
	new = (deg * ratio) + lowerto
	return int(new)

#set servo position given the servo number
def SetPos(number, pos):
	pulseLen = Map(pos, 0, 180, servoMin, servoMax)
	PWMdriver.setPWM(number, 0, pulseLen)	


#TESTING
SetPos(0, 90)
