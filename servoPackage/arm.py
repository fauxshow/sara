#control the robotic arm
import servoDrive

joint1 = 0
joint2 = 1
joint3 = 2
joint4 = 3
joint5 = 4
joint6 = 5

def moveJoint(jointNumber, pos):
	#any calibration can go here
	servoDrive.SetPos(jointNumber, pos)

