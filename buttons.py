#Module for reacting to button presses

import RPi.GPIO as GPIO                    #Import GPIO library
import time                                #Import time library
GPIO.setmode(GPIO.BCM)                     #Set GPIO pin numbering 

buttonA = 20                                  
buttonB = 21                                  

GPIO.setup(buttonA,GPIO.IN)                  
GPIO.setup(buttonB,GPIO.IN)                  


def buttonAPressed():
	return GPIO.input(buttonA)

def buttonBPressed():
	return GPIO.input(buttonB)


#testing
'''
while True:
	print buttonAPressed(), buttonBPressed()
	time.sleep(0.1)
'''
