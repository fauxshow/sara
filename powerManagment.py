from __future__ import division
import time
import RPi.GPIO as GPIO
import Adafruit_ADS1x15

GPIO.setmode(GPIO.BCM)

relayPin = 14
R1 = 4700
R2 = 2700

GPIO.setup(relayPin, GPIO.OUT)

adc = Adafruit_ADS1x15.ADS1115()
GAIN = 1

def calcVoltage(value):
    if(value>0):
    	vOut = 5/32768*value
    	vIn = vOut / (R2/(R1+R2))
    	return vIn
    else:
        return 0

def getBatteryVoltage():
    val = adc.read_adc(0, gain=GAIN)
    return calcVoltage(val)

def mainsConnected():
    val = adc.read_adc(1, gain=GAIN)
    if calcVoltage(val)>4.5:
        return True
    else:
        return False

def batteryPowerOn():
    GPIO.output(relayPin, 1)

def batteryPowerOff():
    GPIO.output(relayPin, 0)

#testing
'''
while True:
    print(getBatteryVoltage(),mainsConnected())
    time.sleep(0.5)
'''
