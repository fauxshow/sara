
from bottle import *
#import camera
import motors
import servoPackage.camMount as camMount

def fetchIP():
	ipString = subprocess.check_output(["ifconfig wlan0| grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"], shell=True);
	return ipString	

@get('/ping/')
def ping():
	motors.ping()
	print "ping received"

#speed [0-127]
#duration [0-255]
@get('/move/<direction>/<speed>/<duration>/')
def move(direction, speed, duration):
	speed = int(speed)
	duration = int(duration)
	if direction=='forward':
		print "Moving "+direction
		motors.fwd()
	elif direction=='reverse':
		print "Moving "+direction		
		motors.rev()
	elif direction=='left':
		print "Moving "+direction		
		motors.left()
	elif direction=='right':
		print "Moving "+direction		
		motors.right()
	elif direction=='stop':
		print "Moving "+direction		
		motors.stop()
	return "ok"

@get('/stop/')
def stop():
	motors.stop()
	print "Stopping"

@get('/movecamyaw/<pos>/')
def moveCamYaw(pos):
	camMount.yaw(int(pos))

@get('/movecampitch/<pos>/')
def moveCamPitch(pos):
	camMount.pitch(int(pos))

'''
@post('/getpic/')
def GetPic():
	try:
		picData = camera.ReadPicture().getvalue()
		print "returning data"
		return picData
	except Exception as e:
		print e
		return 500

@post('/getfaces/')
def GetFaces():
	camera.FindFaces()
	try:
		path = '/home/pi/Ted/images/frameFaces.jpg'
		with open(path, 'rb') as f:
			picData = f.read()
		print "returning data"
		return picData
	except Exception as e:
		print e
		return 500
'''
run(host=fetchIP(), port=8085)
