import smbus
import time

bus = smbus.SMBus(1)
address = 0x04

#validate move speed and dur values
def MoveValid(ls,rs,dur):
	isValid = True
	if ls<-127 or ls>127 or rs<-127 or rs>127:
		print str(ls) + ", " + str(rs)
		isValid = False
	if dur<0 or dur>255:
		print dur
		isValid = False
	return isValid

#for testing, toggle onboard led
def ping():
	write(0, [])	#1 is the command code for the led toggle
	#this method doesnt need to send a value, but we must send something, so we send 0
	usleep()
	response = bus.read_byte(address)
	return response

'''
Move the main drive motors
ls = [-127,128]	(left motor speed)
rs = [-127,128]	(right motor speed)
negative speeds are reverse
'''
def move(ls, rs, dur):
	if MoveValid(ls,rs,dur):
		ls = ls+127
		rs = rs+127
		print "Move str: "+str(ls)+","+str(rs)+"|"+str(dur)
		write(1, [ls,rs,dur]) 	#1 is command code for move
	else:
		print "Error: invalid move speed or time"

def stop():
	write(2, [])

#servo 0 = yaw
#servo 1 = pitch
def MoveCamServo(servo, pos):
	if servo == 'yaw':
		write(3, [pos])
	elif servo == 'pitch':
		write(4, [pos])
	else:
		print "Invalid servo name [yaw,pitch]"



#NEW i2c INTERFACE
def write(command, message):
	bus.write_byte(address, 255)	#begin message
	usleep()
	bus.write_byte(address, command)
	usleep()
	for b in message:
		bus.write_byte(address, b)
		usleep()
	bus.write_byte(address, 254)	#end message	





###OLD i2c interface###

#write 2 bytes to the i2c bus, one command bytes and one value byte
def oldwrite(command, value):
	if(command < 256):
		bus.write_byte(address, command)
		#time.sleep(0.05)
	else:
		print "Invalid command"
	if(value < 256):
		bus.write_byte(address, value)
		#time.sleep(0.05)	
	else:
		print "Invalid value, must be <256"


#write a command followed by a byte stream
#if 0 is passed as the type, the method will expect value to be strng,a nd will be send as an array of chars
#if 1 is passed as the type, the method will expect an array of bytes or integers (obviously < 256)
def oldwriteStream(command, value, type):
	bus.write_byte(address, command)
	usleep()
	bus.write_byte(address, len(value))
	usleep()
	if(type == 0):
		for b in value:
			bus.write_byte(address, ord(b))
			#usleep()
	elif(type == 1):
		for b in value:
			bus.write_byte(address, b)

def sendByte(b):
	bus.write_byte(address, b)


#a small buffer time
def usleep():
	time.sleep(0.1)	



#move(89,9)
