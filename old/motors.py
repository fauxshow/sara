#this is a higher level from comms and imports all its methods
#mainly what we do is implement the movement directions

from comms import *
import time

#convert from ms to the time unit read by the uc
#each unit equates to 100ms, since we only feed it a single byte, we can
#specify anwhere from 0-25500ms in steps of 100ms
def FromMS(seconds):
	return int(seconds*10)

#speeds should be 0-127
def forward(speed, t):
	move(speed,speed,FromMS(t))
def reverse(speed, t):
	move(speed*-1,speed*-1,FromMS(t))
def left(speed, t):
	move(speed,speed*-1,FromMS(t))
def right(speed, t):
	move(speed*-1,speed,FromMS(t))
