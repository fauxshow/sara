from picamera import PiCamera
from picamera.array import PiRGBArray
import visionPackage.faceRec as faceRec
import time
import io

cam = PiCamera()
rawCapture = PiRGBArray(cam)
cam.resolution = (160,120)

time.sleep(0.1)

#take a pic and write to disk
def TakePicture(path):
	cam.capture(path)
	print "took picture"

#take a picture and keep in memory
def ReadPicture():
	rawCapture.truncate(0)		#clear the stream
	stream = io.BytesIO()
	cam.capture(stream, format='jpeg', use_video_port=True)
	
	return stream

#take a picture and keep in memory
def ReadPictureRaw():
	rawCapture.truncate(0)		#clear the stream
	cam.capture(rawCapture, format='bgr', use_video_port=True)
	image = rawCapture.array
	return image

def FindFaces():
	image = ReadPictureRaw()
	results = faceRec.DetectFaces(image, 15)
	print results
	

#TakePicture('/home/pi/Ted/images/frame.jpg')

#ReadPicture()
FindFaces()
