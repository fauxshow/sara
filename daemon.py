import buttons
import neoPixel.ledStrip as ledStrip
import HCSR04
from time import sleep
import speech
import motors
import blockSensors
import servoPackage.arm as arm
import servoPackage.camMount as camMount

SPEED = 0.3			#the speed of the loop

### STARTUP SEQUENCE

#speech.say("Hello Ronan, starting up")
#play LED boot animation
#init all servos

### MAIN LOOP
while True:
	distance = HCSR04.getDistance()
	print(distance)
	if distance>0:
		ledStrip.setAll(ledStrip.wheel(int(distance*2)))
	
	sleep(SPEED)
