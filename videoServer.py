
from bottle import *
import camera

def fetchIP():
	ipString = subprocess.check_output(["ifconfig wlan0| grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"], shell=True);
	return ipString	


@post('/getpic/')
def GetPic():
	try:
		picData = camera.ReadPicture().getvalue()
		print "returning data"
		return picData
	except Exception as e:
		print e
		return 500

@post('/getfaces/')
def GetFaces():
	camera.FindFaces()
	try:
		path = '/home/pi/Ted/images/frameFaces.jpg'
		with open(path, 'rb') as f:
			picData = f.read()
		print "returning data"
		return picData
	except Exception as e:
		print e
		return 500

run(host=fetchIP(), port=8086)
