import time
from neopixel import *


# LED strip configuration:
LED_COUNT      = 8      # Number of LED pixels.
LED_PIN        = 12 	# GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
strip.begin()

class ColorObject():
	def __init__(self,r,g,b):
		self.r = r
		self.g = g
		self.b = b
		self.color = Color(g,r,b)

def setLED(n,color):
	strip.setPixelColor(n, color)
	strip.show()

def setAll(color):
	for i in range(LED_COUNT):	
		strip.setPixelColor(i, color)
	strip.show()

def off():	
	for i in range(LED_COUNT):	
		strip.setPixelColor(i, Color(0,0,0))
	strip.show()

#list of leds, color from, color to, time it will take
def fade(leds, fromColor, toColor, speed):
	currentR = fromColor.r
	currentG = fromColor.g
	currentB = fromColor.b
	for led in leds: setLED(led, fromColor.color)
	totalDist = abs(fromColor.r-toColor.r) + abs(fromColor.g-toColor.g) + abs(fromColor.b-toColor.b)
	stepLen = speed*1.0/totalDist
	while currentG is not toColor.g:
		mod = 1
		if(toColor.g<fromColor.g): mod = -1
		currentG += mod
		for led in leds: setLED(led, Color(currentG, currentR, currentB))
		time.sleep(stepLen)
		strip.show()
	while currentB is not toColor.b:
		mod = 1
		if(toColor.b<fromColor.b): mod = -1
		currentB += mod
		for led in leds: setLED(led, Color(currentG, currentR, currentB))
		time.sleep(stepLen)
		strip.show()
	while currentR is not toColor.r:
		mod = 1
		if(toColor.r<fromColor.r): mod = -1
		currentR += mod
		for led in leds: setLED(led, Color(currentG, currentR, currentB))
		time.sleep(stepLen)
		strip.show()

#animation to play on startup
def startSequence():
	black = ColorObject(0,0,0)
	red = ColorObject(255,0,0)
	yellow = ColorObject(255,60,0)
	green = ColorObject(0,255,0)
	blue = ColorObject(0,0,255)
	white = ColorObject(255,255,255)

	fade([0,7], black, red, 0.5)
	fade([1,6], black, yellow, 0.5)
	fade([2,5], black, green, 0.5)
	fade([3,4], black, blue, 0.5)

	fade([0,7], red, black, 0.3)
	fade([1,6], yellow, black, 0.3)
	fade([2,5], green, black, 0.3)

	fade([3,4], blue, white, 1)
	time.sleep(0.1)
	setLED(3,black.color)
	setLED(4,black.color)
	setLED(2,white.color)
	setLED(5,white.color)
	time.sleep(0.1)
	setLED(2,black.color)
	setLED(5,black.color)
	setLED(1,white.color)
	setLED(6,white.color)
	time.sleep(0.1)
	setLED(1,black.color)
	setLED(6,black.color)
	setLED(0,white.color)
	setLED(7,white.color)
	time.sleep(0.1)
	setLED(0,black.color)
	setLED(7,black.color)


def wheel(pos):
	"""Generate rainbow colors across 0-255 positions."""
	if pos < 85:
		return Color(pos * 3, 255 - pos * 3, 0)
	elif pos < 170:
		pos -= 85
		return Color(255 - pos * 3, 0, pos * 3)
	else:
		pos -= 170
		return Color(0, pos * 3, 255 - pos * 3)

def rainbow(wait_ms=20, iterations=1):
	"""Draw rainbow that fades across all pixels at once."""
	for j in range(256*iterations):
		for i in range(strip.numPixels()):
			strip.setPixelColor(i, wheel((i+j) & 255))
		strip.show()
		time.sleep(wait_ms/1000.0)

def rainbowCycle(wait_ms=20, iterations=5):
	"""Draw rainbow that uniformly distributes itself across all pixels."""
	for j in range(256*iterations):
		for i in range(strip.numPixels()):
			strip.setPixelColor(i, wheel((int(i * 256 / strip.numPixels()) + j) & 255))
		strip.show()
		time.sleep(wait_ms/1000.0)


#testing
#setLED(0, Color(255,255,255))

#startSequence()
