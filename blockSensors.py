#Module for reacting to IR obstruction sensors

import RPi.GPIO as GPIO                    #Import GPIO library
import time                                #Import time library
GPIO.setmode(GPIO.BCM)                     #Set GPIO pin numbering 

sensorA = 5                                  
sensorB = 6                                  

GPIO.setup(sensorA,GPIO.IN)                  
GPIO.setup(sensorB,GPIO.IN)                  


def sensorATriggered():				#for some reason these sensors return LOW when triggered
	return not GPIO.input(sensorA)

def sensorBTriggered():
	return not GPIO.input(sensorB)


#testing

#while True:
#	print sensorATriggered(), sensorBTriggered()
#	time.sleep(0.1)

