import os
import base64
import json

#record audio clip
def record():
	os.system("sox -c 1 -r 16000 -t alsa default clip.flac")
	#encode clip as base64 binary string
	audioFile = open("clip.flac", 'r')
	audio = audioFile.read()
	audio64 = base64.b64encode(audio)

	#open .json request file and insert audio info
	jsonString = "{  'config': {        'encoding':'FLAC',		      'sampleRate':16000,			        'languageCode':'en-US'					  },					    'audio': {						      'content':'"
	with open("sync-request.json", 'w') as f:
		f.write(jsonString+audio64+"'}}")


#create a new auth token and write to file, this doesn't need to be done every time
def authorize():
	os.remove("auth_token")
	os.system("gcloud auth activate-service-account --key-file=SARA-cbd60c8f5a91.json")
	os.system("gcloud auth print-access-token >> auth_token")


#get text from speech
def speech2Text():
	record()
	#authorize()	#only do this if credentials have expired
	
	#read auth token
	authToken = open("auth_token").read()

	#now call the curl command with the auth token
	curlString = "curl -s -k -H \"Content-Type: application/json\" -H \"Authorization: Bearer "+authToken[:-1]+"\" https://speech.googleapis.com/v1beta1/speech:syncrecognize -d @sync-request.json > results.json"
	os.system(curlString)

	#read in the results and parse out actual text
	resultsFile = open("results.json", 'r')
	results = resultsFile.read()
	print results
	if results == "{}":
		return None
	else:
		data = json.loads(results)
		return data['results'][0]['alternatives'][0]['transcript'].lower()


