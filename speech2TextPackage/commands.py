#this is a dictionary of command objects which details voice interactions
import ledset	#we import these libraries here, but we will probably pass the python code string back to a parent class to be executed, where all these methods are accessible

class VoiceCommand():
	def __init__(self, commands, params, action, reply):
		self.commands = commands #a list of valid triggers to match this command, # in the string represents a wildcard 
		self.params = params
		self.action = action	 #a string containing python code to execute, # will indicate where the code should be modified by the params
		self.reply = reply		 #a reply to speak

	#ensure the passed param is valid
	def checkParams(self, passedParam):
		valid = False
		for p in self.params:
			if p == passedParam:
				valid = True
		return valid


	#generate executable code here
	def execute(self, params):
		#modify code to be changed by param in whatever way
		#find #s in execute code and insert passed params
		words = self.action.split('#')
		newString = ""
		if (len(words)-1 == len(params)):
			for i in range(len(params)):
				words[i]+=params[i]
			for w in words:
				newString += w
		else:
			print "Incorrect number of params"
			return None
		return newString



ledStrip = VoiceCommand(["turn lights #","turn the lights #","turn light #","turn the light #"], ["on","off","red","blue","green","orange","white"], "ledset.setColourName('#')", "Turning lights #")
nameResponse = VoiceCommand(["what's your name","tell me your name","what is your name","who are you"],[],"speech.say('My name is Sara')","")

VoiceCommands = [ledStrip, nameResponse]


#find the locations of the params in the command
def getParamLocs(whichCommand):		
	paramLocs = []
	i = 0
	for w in whichCommand.split(' '):
		if w == '#':	
			paramLocs.append(i)
		i+=1
	return paramLocs

#parse out the params at locs
def getParams(locs, command):
	params = []
	for l in locs:
		params.append(command[l])
	return params

#look for a match and carry it out, text must be all lowercase
def findCommand(rawText):
	rtWords = rawText.split(' ')
	matchedCommand = None	#this is the command object
	whichCommand = -1		#this is the specific command string it matched with
	found = False
	for vc in VoiceCommands:
		if not found:
			for c in vc.commands:
				if not found:
					match = True
					cWords = c.split(' ')
					if len(rtWords) != len(cWords):
						match = False
					else:
						for i in range(len(cWords)):
							if rtWords[i] != cWords[i] and cWords[i] != '#':
								match = False
					if match: 
						matchedCommand = vc
						whichCommand = c
						found = True
						print("Match found")
	
	print whichCommand
	if matchedCommand is not None:
		paramLocs = getParamLocs(whichCommand)
		params = getParams(paramLocs, rtWords)
		print params	
		return matchedCommand.execute(params)
	else:
		print "Command not recognised"
		return None

#findCommand("turn the lights blue")
					
				
	
