import requests
import sys

room = '70'
kitchen = '89'

loc = room



def setColour(red,green,blue):
    url = 'http://192.168.1.'+loc+'/?cmd=colour&red=' + str(red) + "&green=" + str(green) + "&blue=" + str(blue)
    r = requests.get(url)
    if r.status_code == 200:
        print "Success"
    else:
        print r.status_code

def setColourName(name):
	if name == 'on': setColour(255,50,0)
	if name == 'off': setColour(0,0,0)
	if name == 'red': setColour(255,0,0)
	if name == 'blue': setColour(0,0,255)
	if name == 'green': setColour(0,255,0)
	if name == 'orange': setColour(255,50,0)
	if name == 'white': setColour(255,220,240)

'''
print "ok"

red = int(sys.argv[1])
green = int(sys.argv[2])
blue = int(sys.argv[3])

setColour(red,green,blue)
'''
