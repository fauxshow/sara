import colourDetect
import faceRec
import picamera
import datetime

cam = picamera.PiCamera()

#take uniquely named photo
def takePic():
	t = datetime.datetime.now()
	name = str(t)+".jpg"
	cam.capture(name)
	print "Image captured"


#capture a discardable frame
def takeFrame():
	cam.capture('frame.jpg')
	print "Frame captured"

def getFaces():
	results = faceRec.DetectFaces('frame.jpg', 30)	
	print "Faces found: "+str(len(results))

def getObjects():
	results = colourDetect.GetObjects('frame.jpg', [255,0,0], 100, 10)
	print "Objects found: "+str(len(results))
	
