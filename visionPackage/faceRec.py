import cv2
#import sys

#imgpath = sys.argv[1]
#cascpath = sys.argv[2]


class Face():
	def __init__(self,x,y,w,h):
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.cx = x+(w/2)
		self.cy = y+(h/2)

def DetectFaces(image, sizeThres):
	#cascpath = '/home/pi/Ted/visionPackage/data/haarcascade_frontalface_default.xml'
	cascpath = '/home/pi/Ted/visionPackage/data/haarcascade_upperbody.xml'
	#cascpath = 'data/haarcascade_eye.xml'
	faceCascade = cv2.CascadeClassifier(cascpath)

	#image = cv2.imread(frame)		#was reading from disk but now getting raw image

	results = []

	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

	faces = faceCascade.detectMultiScale(
		gray,
		scaleFactor=1.1,
		minNeighbors=5,
		minSize=(sizeThres, sizeThres),
		flags = cv2.cv.CV_HAAR_SCALE_IMAGE
	)


	for (x, y, w, h) in faces:
		new = Face(x,y,w,h)
		results.append(new)

	for f in results:
		coords = "["+str(f.cx)+","+str(f.cy)+"]"
		print coords
	
		cv2.putText(image, coords , (f.cx,f.cy), cv2.FONT_HERSHEY_SIMPLEX, 0.3, 255)
		cv2.rectangle(image, (f.x, f.y), (f.x+f.w, f.y+f.h), (0, 255, 0), 2)

	cv2.imwrite("/home/pi/Ted/images/frameFaces.jpg",image)

	return results


#DetectFaces()
