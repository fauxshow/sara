import cv2
import numpy as np
import time
from copy import deepcopy
#cap = cv2.VideoCapture(0)

class Object():
	def __init__(self,x,y,w,h,colour):
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.colour = colour
'''
#given a colour name, return uppder and lower bound HSV values
def GetColour(colour):
	ub = None
	lb = None
	if colour == 'blue':
		lb = [100,150,50]
		ub = [130,255,255]
	elif colour == 'red':
		lb = [0,50,50]
		ub = [10,255,255]
	elif colour == 'green':
		lb = [40,50,50]
		ub = [100,255,255]
	elif colour == 'orange':	
		lb = [10,50,50]
		ub = [20,255,255]

	lower =  np.array(lb, dtype=np.uint8)
	upper = np.array(ub, dtype=np.uint8)
	return lower, upper
'''

#get the lower and upper HSV bounds of an RGB colour value
#higher colour tolerance will allow a wider rang eof colours ~[5-20]
#higher saturdation tolerance should allow a wider range of saturations ~[0-150]
def GetColour(colour, huetolerance, sattolerance):
	ub = [0,0,0]
	lb = [0,0,0]

	#colour = [r,g,b]
	colour = np.uint8([[[colour[0],colour[1],colour[2]]]])
	colour = cv2.cvtColor(colour, cv2.COLOR_BGR2HSV)

	ub[0] = colour[0][0][0] + huetolerance
	ub[1] = 255
	ub[2] = 255
	lb[0] = colour[0][0][0] - huetolerance
	lb[1] = colour[0][0][1] - sattolerance
	lb[2] = colour[0][0][2] - sattolerance

	#constrain hue
	if ub[0] > 179:
		ub[0] = 179
	if lb[0] < 0:
		lb[0] = 0

	#print ub
	#print lb
	
	lower =  np.array(lb, dtype=np.uint8)
	upper = np.array(ub, dtype=np.uint8)
	return lower, upper



def GetObjects(frame, colour, sizeThres, maxObj):
	objects = []

	#frame = cv2.imread('image.jpg')


	# Take each frame

	#_, frame = cap.read()

	# Convert BGR to HSV
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	lowerBound, upperBound = GetColour(colour,10,150)
	mask = cv2.inRange(hsv, lowerBound, upperBound)

	# Bitwise-AND mask and original image
	res = cv2.bitwise_and(frame,frame, mask= mask)

	#detect contours
	contours, hier = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

	#narrow contours down to more complex ones(which are prabbly bigger objects rather than just points)
	cont = []
	for ct in contours:
		if len(ct) > sizeThres:
			cont.append(ct)

	#sort the points in each contour to get x and y, w and h
	for ct in cont:

		xsort = sorted(ct,key=lambda i: i[0][0])
		ysort = sorted(ct,key=lambda i: i[0][1])
		x = xsort[0][0][0]
		y = ysort[0][0][1]
		w = xsort[-1][0][0] - x
		h = ysort[-1][0][1] - y

		#print x,y,w,h
		new = Object(x,y,w,h,colour)
		objects.append(new)

	#remove objects which are inside the bounds of larger objects
	nonOverlapping = []
	for o in objects:
		overlapping = False
		for oi in objects:
			if o.x>oi.x and o.y<oi.y and (o.x+o.w)<(oi.x+oi.w) and (o.y+o.h)>(oi.y+oi.h):
				overlapping = True
				print "found a contiained one"
		if overlapping == False:
			nonOverlapping.append(o)


	return nonOverlapping

	#for o in objects:
	#	cv2.rectangle(frame, (o.x,o.y) , (o.x+o.w,o.y+o.h) , (9,255,0),2)

	#cv2.imshow('frame',frame)
	#cv2.imshow('mask',mask)
	#cv2.imshow('frame',frame)

	#cv2.waitKey(0)


